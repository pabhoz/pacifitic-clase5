class Player extends DOMElement{
    constructor(src, ...params){
        super();
        this._src = src;
        this._controls = false;
        this._element = null;
        this.initDOMElement();
        this.DOMElement.ontimeupdate = () => this.updateElement();
    }

    set element(value){
      this._element = value;
    }

    get element(){
      return this._element;
    }

    get src(){
        return this._src;
    }

    set src (value){
        this._src = value;
        if(this.DOMElement != null){
          this.DOMElement.src = this.src;
        }
    }

    get controls(){
        return this._controls;
    }

    set controls (value){
        this._controls = value;
    }

    initDOMElement(){
        var playerE = document.createElement("audio");
        playerE.src = this.src;
        playerE.controls = this.controls;
        var playerDiv = document.createElement("div");
        playerDiv.id = "player";
        var controls = document.createElement("div");
        controls.classList.add("controls");

        var play = document.createElement("div");
        play.classList.add("playBtn");
        play.innerHTML = "&#9658;";
        play.onclick = () => this.play();

        var stop = document.createElement("div");
        stop.classList.add("stopBtn");
        stop.innerHTML = "&#9611;";

        controls.appendChild(play);
        controls.appendChild(stop);

        playerDiv.appendChild(playerE);
        playerDiv.appendChild(controls);

        this._DOMElement = playerDiv;
    }

    play(){
        try{
            if(this.DOMElement.querySelector("audio").paused){
              this.DOMElement.querySelector("audio").play();
              this.DOMElement.querySelector(".playBtn").innerHTML = `&#9646;&#9646;`;
            }else{
              this.pause();
            }
        } catch (error){
            console.log(error);
        }
    }
    pause(){
        try{
            this.DOMElement.querySelector("audio").pause();
        } catch (error){
            console.log(error);
        }
    }
    stop(){
      this.pause();
      this.DOMElement.querySelector("audio").currentTime = 0;
    }

    updateElement(){
      if(this.element != null){
        var w = (this.DOMElement.currentTime / this.DOMElement.duration) * 100;
        console.log(w);
        var el = this.element.DOMElement.querySelector(".progressBar");
        el.style.width = `${w}%`;
      }
    }
}
